const ReactRefreshWebpackPlugin = require('@pmmmwh/react-refresh-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const { merge } = require('webpack-merge');

function sslServe(config) {
  const isHttps = config.env.envVar('WDS_SSL') === 'true';
  const sslKeyFile = config.env.envVar('WDS_SSL_KEY_FILE');
  const sslCertFile = config.env.envVar('WDS_SSL_CERT_FILE');
  if (isHttps && sslKeyFile !== '' && sslCertFile !== '') {
    return {
      devServer: {
        client: {
          webSocketURL: {
            protocol: 'wss',
            hostname: 'localhost',
          },
        },
        server: {
          type: 'https',
          options: {
            key: config.rootDir(sslKeyFile),
            cert: config.rootDir(sslCertFile),
          },
        },
      },
    };
  }
  return {};
}

function serve(config) {
  if (config.serve) {
    return merge(
      {
        entry: './src/dev/index',
        devServer: {
          allowedHosts: 'all',
          devMiddleware: {
            publicPath: config.publicPath,
          },
          headers: {
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Methods':
              'GET, POST, PUT, DELETE, PATCH, OPTIONS',
            'Access-Control-Allow-Headers':
              'X-Requested-With, content-type, Authorization',
          },
          historyApiFallback: {
            index: config.publicPath,
          },
          hot: true,
          liveReload: false,
          static: config.destDir(),
        },
        // Dev Server tries to combine "main" and "remote-entry" in weird ways.
        // Splitting async chunks only fixes this.
        optimization: {
          splitChunks: {
            chunks: 'async',
          },
        },
        plugins: [
          new HtmlWebpackPlugin({
            title: `${config.project.name} Demo`,
            template: config.srcDir('dev', 'index.ejs'),
            chunks: ['main'],
            publicPath: config.publicPath,
          }),
          new ReactRefreshWebpackPlugin(),
        ],
      },
      sslServe(config),
    );
  }
  return {};
}

module.exports = { serve };
