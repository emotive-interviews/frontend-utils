const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const { base } = require('./base');
const { federationPlugin } = require('./federation-plugin');
const { manifestPlugin } = require('./manifest-plugin');
const { serve } = require('./serve');

function selectBabelLoader(config) {
  return config.serve
    ? [
        {
          loader: require.resolve('babel-loader'),
          options: {
            plugins: [require.resolve('react-refresh/babel')],
          },
        },
      ]
    : require.resolve('babel-loader');
}

function babel(config) {
  return {
    module: {
      rules: [
        {
          test: /\.(jsx?|tsx?)$/,
          exclude: /node_modules/,
          use: selectBabelLoader(config),
        },
      ],
    },
  };
}

function css(config) {
  const isProduction = config.nodeEnv === 'production';
  const cssLoaderOptions = {
    modules: {
      auto: true,
      localIdentHashSalt: config.project.camelCase,
      localIdentName: isProduction ? '[hash:base64]' : '[path][name]__[local]',
    },
    sourceMap: false,
  };
  return {
    module: {
      rules: [
        {
          test: /\.css$/i,
          use: [
            isProduction
              ? MiniCssExtractPlugin.loader
              : require.resolve('style-loader'),
            {
              loader: require.resolve('css-loader'),
              options: {
                ...cssLoaderOptions,
                importLoaders: 1,
              },
            },
            require.resolve('postcss-loader'),
          ],
        },
        {
          test: /\.(scss|sass)$/i,
          use: [
            isProduction
              ? MiniCssExtractPlugin.loader
              : require.resolve('style-loader'),
            {
              loader: require.resolve('css-loader'),
              options: {
                ...cssLoaderOptions,
                importLoaders: 2,
              },
            },
            require.resolve('postcss-loader'),
            require.resolve('sass-loader'),
          ],
        },
      ],
    },
    ...(isProduction
      ? {
          plugins: [
            new MiniCssExtractPlugin({
              filename: '[name].[contenthash].css',
              chunkFilename: '[id].[contenthash].css',
              attributes: {
                'data-id': config.project.paramCase,
              },
            }),
          ],
        }
      : {}),
  };
}

function environment(config) {
  const { nodeEnv } = config;
  return nodeEnv === 'development'
    ? {
        devtool: 'inline-source-map',
        mode: 'development',
      }
    : {
        devtool: 'source-map',
        mode: 'production',
      };
}

function fonts() {
  return {
    module: {
      rules: [
        {
          test: /\.(woff|woff2|eot|ttf|otf)$/i,
          type: 'asset/resource',
        },
      ],
    },
  };
}

function images() {
  return {
    module: {
      rules: [
        {
          test: /\.(png|jpg|jpeg|gif)$/i,
          type: 'asset/resource',
        },
      ],
    },
  };
}

function resolveFromSource(config) {
  return {
    resolve: {
      modules: [
        'node_modules',
        config.rootDir('node_modules'),
        config.srcDir(),
      ],
    },
  };
}

// deprecated: Here just to compatibility with previous versions.
function sass() {
  return {};
}

function svg(config) {
  // SVGR options, copied from create-react-app.
  // SVGR options docs: https://react-svgr.com/docs/options/
  // Config from cra: https://github.com/facebook/create-react-app/blob/main/packages/react-scripts/config/webpack.config.js
  const SVGR_OPTIONS = {
    prettier: false, // skips Prettier formatting.
    svgo: false, // skips default svgo
    svgoConfig: {
      plugins: [{ removeViewBox: false }],
    },
    titleProp: true, // adds a title prop
    ref: true, // forwards ref to root element of svg
  };

  return {
    module: {
      rules: [
        {
          test: /\.icon\.svg$/i,
          include: config.srcDir(),
          use: [
            {
              loader: require.resolve('@svgr/webpack'),
              options: {
                icon: true,
                ...SVGR_OPTIONS,
              },
            },
            require.resolve('url-loader'),
          ],
        },
        {
          test: /\.svg$/i,
          oneOf: [
            {
              include: config.srcDir(),
              use: [
                {
                  loader: require.resolve('@svgr/webpack'),
                  options: SVGR_OPTIONS,
                },
                require.resolve('url-loader'),
              ],
            },
            {
              type: 'asset/resource',
            },
          ],
        },
      ],
    },
  };
}

module.exports = {
  babel,
  base,
  css,
  environment,
  federationPlugin,
  fonts,
  images,
  manifestPlugin,
  resolveFromSource,
  sass,
  serve,
  svg,
};
