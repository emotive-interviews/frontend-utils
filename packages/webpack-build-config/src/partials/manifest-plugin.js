const { WebpackManifestPlugin } = require('webpack-manifest-plugin');

function manifestPlugin(config) {
  return {
    plugins: [
      new WebpackManifestPlugin({
        publicPath: config.publicPath,
        fileName: 'asset-manifest.json',
        generate: (seed, files, entrypoints) => {
          const manifestFiles = files.reduce((manifest, file) => {
            manifest[file.name] = file.path;
            return manifest;
          }, seed);
          return {
            files: manifestFiles,
            entrypoints: entrypoints,
          };
        },
      }),
    ],
  };
}

module.exports = { manifestPlugin };
