const { readdirSync } = require('fs');
const { basename, extname, relative } = require('path');
const { ModuleFederationPlugin } = require('webpack').container;

function createExposedInterfaceConfig(config) {
  try {
    const interfaceFiles = readdirSync(config.srcDir('interfaces'));
    const relativePath = relative(
      config.rootDir(),
      config.srcDir('interfaces'),
    );
    return (
      interfaceFiles
        // Remove the file extension
        .map((interfaceFile) => basename(interfaceFile, extname(interfaceFile)))
        // Skip test files
        .filter((interfaceFile) => !interfaceFile.endsWith('.test'))
        .reduce(
          (exposeMap, interfaceFile) => ({
            ...exposeMap,
            [`./${interfaceFile}`]: `./${relativePath}/${interfaceFile}`,
          }),
          {},
        )
    );
  } catch (e) {
    return undefined;
  }
}

// Dependencies that should always be shared as a singleton (one instance on the page at any time)
const SINGLETON_DEPS = ['react', 'react-dom', '@emotion/react'];

// Dependencies that, when defined, should be shared.
const DEFAULT_SHARED_DEPS = ['react', 'react-dom', '@emotion/react'];

function createSharedConfig(config, settings = {}) {
  const { shared = [] } = settings;
  const { dependencies = [] } = config.pkg;

  const combinedSharedDeps = shared.concat(DEFAULT_SHARED_DEPS);

  return Object.entries(dependencies)
    .filter(([depName]) => combinedSharedDeps.includes(depName))
    .reduce((acc, [depName, version]) => {
      const newDep = SINGLETON_DEPS.includes(depName)
        ? { singleton: true, requiredVersion: version }
        : version;
      return { ...acc, [depName]: newDep };
    }, {});
}

function createRemotesConfig(config, settings = {}) {
  const { remotes = [] } = settings;
  return remotes.reduce((acc, curr) => {
    return {
      ...acc,
      [`@emotive/${curr}`]: `promise window.emotiveLoader.loadFederatedModuleContainer('${curr}')`,
    };
  }, {});
}

function federationPlugin(config, settings = {}) {
  const { federationPlugin: partialSettings = {} } = settings;
  const exposes = createExposedInterfaceConfig(config);

  if (!exposes) {
    return {};
  }

  const remotes = createRemotesConfig(config, partialSettings);
  const shared = createSharedConfig(config, partialSettings);

  return {
    plugins: [
      new ModuleFederationPlugin({
        name: `E${config.project.camelCase}`,
        filename: 'remote-entry.js',
        exposes,
        remotes,
        shared,
      }),
    ],
  };
}

module.exports = { federationPlugin };
