const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const { DefinePlugin, IgnorePlugin } = require('webpack');

function base(config) {
  return {
    context: config.rootDir(),
    entry: './src/index',
    optimization: {
      splitChunks: {
        chunks: 'all',
      },
    },
    output: {
      publicPath: 'auto',
      path: config.destDir(),
      filename: '[name].[contenthash].js',
    },
    plugins: [
      new CleanWebpackPlugin(),
      new DefinePlugin(config.env.serialize()),
      // Ignore moment locales, because moment is packaged strangely.
      new IgnorePlugin({
        resourceRegExp: /^\.\/locale$/,
        contextRegExp: /moment/,
      }),
    ],
    resolve: {
      extensions: ['.tsx', '.ts', '.jsx', '.js', '.json'],
    },
    target: 'web',
  };
}

module.exports = { base };
