const { loadEnvironment } = require('./load-environment');

function createEnvironment(
  rootPath,
  { nodeEnv = 'production', buildEnv = process.env.BUILD_ENV } = {},
) {
  const envVars = loadEnvironment(rootPath, { buildEnv, nodeEnv });

  return {
    envVar(varName, defaultValue) {
      return envVars[varName] ?? defaultValue;
    },
    serialize() {
      return {
        'process.env': Object.entries(envVars).reduce((env, [key, value]) => {
          env[key] = JSON.stringify(value);
          return env;
        }, {}),
      };
    },
  };
}

module.exports = { createEnvironment };
