const path = require('path');

const fromDir =
  (...root) =>
  (...paths) =>
    path.resolve.apply(null, root.concat(paths));

module.exports = { fromDir };
