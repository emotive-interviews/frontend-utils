const { createBuildConfig } = require('./build-config');

jest.mock('/my-project/package.json', () => ({ name: '@emotive/my-app' }), {
  virtual: true,
});

afterEach(() => {
  jest.clearAllMocks();
});

function createEnvMock() {
  return {
    envVar: jest.fn(),
    serialize: jest.fn(),
  };
}

test('configures the pkg property', () => {
  const rootPath = '/my-project';

  const result = createBuildConfig(rootPath, createEnvMock());

  expect(result.pkg).toEqual(require(`${rootPath}/package.json`));
});

test('configures project names', () => {
  const rootPath = '/my-project';

  const result = createBuildConfig(rootPath, createEnvMock());

  expect(result.project).toMatchInlineSnapshot(`
    Object {
      "camelCase": "myApp",
      "name": "my-app",
      "paramCase": "my-app",
    }
  `);
});

test('provides the env value', () => {
  const rootPath = '/my-project';
  const env = createEnvMock();

  const result = createBuildConfig(rootPath, env);

  expect(result.env).toEqual(env);
});

test('configures default values', () => {
  const rootPath = '/my-project';

  const result = createBuildConfig(rootPath, createEnvMock());

  expect(result.nodeEnv).toEqual('production');
  expect(result.build).toEqual(false);
  expect(result.serve).toEqual(false);
});

test.each([['production'], ['test'], ['development']])(
  'configures nodeEnv to %s',
  (nodeEnv) => {
    const rootPath = '/my-project';

    const result = createBuildConfig(rootPath, createEnvMock(), { nodeEnv });

    expect(result.nodeEnv).toEqual(nodeEnv);
  },
);

test.each([
  [false, false],
  [true, false],
  [false, true],
  [true, true],
])('configures build=%s, serve=%s', (WEBPACK_BUILD, WEBPACK_SERVE) => {
  const rootPath = '/my-project';

  const result = createBuildConfig(
    rootPath,
    createEnvMock(),
    { nodeEnv: 'production' },
    { WEBPACK_BUILD, WEBPACK_SERVE },
  );

  expect(result.build).toEqual(WEBPACK_BUILD);
  expect(result.serve).toEqual(WEBPACK_SERVE);
});

test('configures publicPath', () => {
  const rootPath = '/my-project';

  const result = createBuildConfig(rootPath, createEnvMock());

  expect(result.publicPath).toEqual('/my-app/');
});

test('configures rootDir', () => {
  const rootPath = '/my-project';

  const { rootDir } = createBuildConfig(rootPath, createEnvMock());

  expect(rootDir()).toEqual('/my-project');
  expect(rootDir('utils')).toEqual('/my-project/utils');
  expect(rootDir('utils', 'stuff.js')).toEqual('/my-project/utils/stuff.js');
});

test('configures srcDir', () => {
  const rootPath = '/my-project';

  const { srcDir } = createBuildConfig(rootPath, createEnvMock());

  expect(srcDir()).toEqual('/my-project/src');
  expect(srcDir('components')).toEqual('/my-project/src/components');
  expect(srcDir('utils', 'stuff.js')).toEqual('/my-project/src/utils/stuff.js');
});

test('configures destDir', () => {
  const rootPath = '/my-project';

  const { destDir } = createBuildConfig(rootPath, createEnvMock());

  expect(destDir()).toEqual('/my-project/dist/my-app');
  expect(destDir('public')).toEqual('/my-project/dist/my-app/public');
  expect(destDir('assets', 'image.png')).toEqual(
    '/my-project/dist/my-app/assets/image.png',
  );
});
