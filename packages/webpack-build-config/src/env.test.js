const { vol } = require('memfs');
const { createEnvironment } = require('./env');
const {
  createCleanupEnvFunction,
  toEnvFile,
  toEnvFileVolume,
} = require('./test-helpers');

jest.mock('fs');

const cleanup = createCleanupEnvFunction();

beforeEach(() => {
  vol.reset();
});

afterEach(() => {
  cleanup();
});

describe('createEnvironment', () => {
  test('envVar returns the loaded value', () => {
    const envDir = toEnvFileVolume({
      '.env': toEnvFile({ VALUE: 'my value' }),
    });

    const { envVar } = createEnvironment(envDir);

    expect(envVar('VALUE')).toEqual('my value');
  });

  test('envVar returns the default value when not found', () => {
    const envDir = toEnvFileVolume({
      '.env': toEnvFile({}),
    });

    const { envVar } = createEnvironment(envDir);

    expect(envVar('VALUE', 'default value')).toEqual('default value');
  });

  test('NODE_ENV defaults to "production"', () => {
    const envDir = toEnvFileVolume({});

    const { envVar } = createEnvironment(envDir);

    expect(envVar('NODE_ENV')).toEqual('production');
  });

  test.each([['production'], ['test'], ['development']])(
    'envVar returns correct NODE_ENV value for nodeEnv="%s"',
    (nodeEnv) => {
      const envDir = toEnvFileVolume({});

      const { envVar } = createEnvironment(envDir, { nodeEnv });

      expect(envVar('NODE_ENV')).toEqual(nodeEnv);
    },
  );

  test.each([['review'], ['dev'], ['stg'], ['prd']])(
    'passes along the buildEnv',
    (buildEnv) => {
      const envDir = toEnvFileVolume({
        [`.env.build.${buildEnv}`]: toEnvFile({ VALUE: 'hello' }),
      });

      const { envVar } = createEnvironment(envDir, { buildEnv });

      expect(envVar('VALUE')).toEqual('hello');
    },
  );

  test('serialize correctly serializes loaded environment variables', () => {
    const envDir = toEnvFileVolume({
      '.env': toEnvFile({ VALUE: 'my value', VALUE_2: 'another value' }),
      '.env.defaults': toEnvFile({
        SOME_DEFAULT: 'a value',
        VALUE: 'you will never see me!',
      }),
    });

    const { serialize } = createEnvironment(envDir);

    expect(serialize()).toMatchInlineSnapshot(`
      Object {
        "process.env": Object {
          "CDN_HOSTNAME": "\\"\\"",
          "NODE_ENV": "\\"production\\"",
          "SOME_DEFAULT": "\\"a value\\"",
          "VALUE": "\\"my value\\"",
          "VALUE_2": "\\"another value\\"",
          "WDS_SSL": "\\"\\"",
          "WDS_SSL_CERT_FILE": "\\"\\"",
          "WDS_SSL_KEY_FILE": "\\"\\"",
        },
      }
    `);
  });
});
