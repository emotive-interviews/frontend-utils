const path = require('path');
const { camelCase, paramCase } = require('change-case');
const { fromDir } = require('./utils');

function createBuildConfig(
  rootPath,
  env,
  { nodeEnv = 'production' } = {},
  { WEBPACK_BUILD = false, WEBPACK_SERVE = false } = {},
) {
  const pkg = require(`${rootPath}/package.json`);
  const projectName = path.basename(pkg.name);
  return {
    env,
    nodeEnv,
    build: WEBPACK_BUILD,
    serve: WEBPACK_SERVE,
    project: {
      name: projectName,
      camelCase: camelCase(projectName),
      paramCase: paramCase(projectName),
    },
    publicPath: `/${paramCase(projectName)}/`,
    rootDir: fromDir(rootPath),
    srcDir: fromDir(rootPath, 'src'),
    destDir: fromDir(rootPath, 'dist', paramCase(projectName)),
    pkg,
  };
}

module.exports = { createBuildConfig };
