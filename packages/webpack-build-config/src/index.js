const corePartials = require('./partials');
const { createConfigFrom } = require('./create-config-from');

const createWebpackConfig = createConfigFrom(
  corePartials.base,
  corePartials.environment,
);

const createDefaultWebappConfig = createConfigFrom(
  corePartials.base,
  corePartials.environment,
  corePartials.babel,
  corePartials.css,
  corePartials.fonts,
  corePartials.images,
  corePartials.svg,
  corePartials.federationPlugin,
  corePartials.manifestPlugin,
  corePartials.serve,
);

const createLegacyWebappConfig = createConfigFrom(
  corePartials.base,
  corePartials.environment,
  corePartials.babel,
  corePartials.css,
  corePartials.fonts,
  corePartials.images,
  corePartials.svg,
  corePartials.federationPlugin,
  corePartials.manifestPlugin,
  corePartials.resolveFromSource,
  corePartials.serve,
);

module.exports = {
  createWebpackConfig,
  createDefaultWebappConfig,
  createLegacyWebappConfig,
  partials: corePartials,
};
