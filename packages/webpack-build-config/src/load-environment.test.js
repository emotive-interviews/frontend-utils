const { vol } = require('memfs');
const { loadEnvironment } = require('./load-environment');
const {
  createCleanupEnvFunction,
  toEnvFile,
  toEnvFileVolume,
} = require('./test-helpers');

jest.mock('fs');

const cleanup = createCleanupEnvFunction();

beforeEach(() => {
  vol.reset();
});

afterEach(() => {
  cleanup();
});

describe('loadEnvironment', () => {
  test('CDN_HOSTNAME defaults to ""', () => {
    const envDir = toEnvFileVolume({});

    const result = loadEnvironment(envDir, { nodeEnv: 'production' });

    expect(result).toMatchObject({
      CDN_HOSTNAME: '',
    });
  });

  test.each([['production'], ['test'], ['development']])(
    'NODE_ENV set to "%s"',
    (nodeEnv) => {
      const envDir = toEnvFileVolume({});
      const result = loadEnvironment(envDir, { nodeEnv });
      expect(result).toMatchObject({
        NODE_ENV: nodeEnv,
      });
    },
  );

  test('returns the correct value for single file', () => {
    const envDir = toEnvFileVolume({
      '.env': toEnvFile({ VALUE_1: 'my value' }),
    });

    const result = loadEnvironment(envDir, { nodeEnv: 'production' });

    expect(result).toMatchObject({
      VALUE_1: 'my value',
    });
  });

  test('handles expanded values', () => {
    const envDir = toEnvFileVolume({
      '.env': toEnvFile({ VALUE_1: 'my value', VALUE_2: '$VALUE_1' }),
    });

    const result = loadEnvironment(envDir, { nodeEnv: 'production' });

    expect(result).toMatchObject({
      VALUE_1: 'my value',
      VALUE_2: 'my value',
    });
  });

  test('handles default property files', () => {
    const envDir = toEnvFileVolume({
      '.env': toEnvFile({ VALUE_1: 'my value' }),
      '.env.defaults': toEnvFile({
        VALUE_1: 'default value',
        PROP: 'another property',
      }),
    });

    const result = loadEnvironment(envDir, { nodeEnv: 'production' });

    expect(result).toMatchObject({
      VALUE_1: 'my value',
      PROP: 'another property',
    });
  });

  test('resolves correct value with multiple files', () => {
    const envDir = toEnvFileVolume({
      '.env.production.local': toEnvFile({ VALUE: '.env.production.local' }),
      '.env.local': toEnvFile({ VALUE: '.env.local' }),
      '.env.production': toEnvFile({ VALUE: '.env.production' }),
      '.env': toEnvFile({ VALUE: '.env' }),
      '.env.defaults': toEnvFile({ VALUE: '.env.defaults' }),
    });

    const result = loadEnvironment(envDir, { nodeEnv: 'production' });

    expect(result.VALUE).toEqual('.env.production.local');
  });

  test('ignores .env.local when nodeEnv is "test"', () => {
    const envDir = toEnvFileVolume({
      '.env.local': toEnvFile({ VALUE: '.env.local' }),
      '.env.test': toEnvFile({ VALUE: '.env.test' }),
      '.env': toEnvFile({ VALUE: '.env' }),
      '.env.defaults': toEnvFile({ VALUE: '.env.defaults' }),
    });

    const result = loadEnvironment(envDir, { nodeEnv: 'test' });

    expect(result.VALUE).toEqual('.env.test');
  });

  test.each([['review'], ['dev'], ['stg'], ['prd']])(
    'loads correct vars when buildEnv is "%s"',
    (buildEnv) => {
      const envDir = toEnvFileVolume({
        [`.env.build.${buildEnv}`]: toEnvFile({
          VALUE_1: `.env.build.${buildEnv}`,
        }),
        '.env.production': toEnvFile({ VALUE_2: '.env.production' }),
        '.env': toEnvFile({ VALUE_3: '.env' }),
        '.env.defaults': toEnvFile({ VALUE_4: '.env.defaults' }),
      });

      const result = loadEnvironment(envDir, {
        buildEnv,
        nodeEnv: 'production',
      });

      expect(result).toMatchObject({
        VALUE_1: `.env.build.${buildEnv}`,
        VALUE_2: '.env.production',
        VALUE_3: '.env',
        VALUE_4: '.env.defaults',
      });
    },
  );

  test('prefers buildEnv files to nodeEnv files', () => {
    const envDir = toEnvFileVolume({
      '.env.build.prd': toEnvFile({ VALUE: '.env.build.prd' }),
      '.env.production': toEnvFile({ VALUE: '.env.production' }),
    });

    const result = loadEnvironment(envDir, {
      buildEnv: 'prd',
      nodeEnv: 'production',
    });

    expect(result).toMatchObject({
      VALUE: '.env.build.prd',
    });
  });

  test('loads correct vars when nodeEnv is "production"', () => {
    const envDir = toEnvFileVolume({
      '.env.production.local': toEnvFile({ VALUE_1: '.env.production.local' }),
      '.env.local': toEnvFile({ VALUE_2: '.env.local' }),
      '.env.production': toEnvFile({ VALUE_3: '.env.production' }),
      '.env': toEnvFile({ VALUE_4: '.env' }),
      '.env.defaults': toEnvFile({ VALUE_5: '.env.defaults' }),
    });

    const result = loadEnvironment(envDir, { nodeEnv: 'production' });

    expect(result).toMatchInlineSnapshot(`
      Object {
        "CDN_HOSTNAME": "",
        "NODE_ENV": "production",
        "VALUE_1": ".env.production.local",
        "VALUE_2": ".env.local",
        "VALUE_3": ".env.production",
        "VALUE_4": ".env",
        "VALUE_5": ".env.defaults",
        "WDS_SSL": "",
        "WDS_SSL_CERT_FILE": "",
        "WDS_SSL_KEY_FILE": "",
      }
    `);
  });

  test('loads correct var when nodeEnv is "test"', () => {
    const envDir = toEnvFileVolume({
      '.env.test.local': toEnvFile({ VALUE_1: '.env.test.local' }),
      '.env.local': toEnvFile({ VALUE_2: '.env.local' }),
      '.env.test': toEnvFile({ VALUE_3: '.env.test' }),
      '.env': toEnvFile({ VALUE_4: '.env' }),
      '.env.defaults': toEnvFile({ VALUE_5: '.env.defaults' }),
    });

    const result = loadEnvironment(envDir, { nodeEnv: 'test' });

    expect(result).toMatchInlineSnapshot(`
      Object {
        "CDN_HOSTNAME": "",
        "NODE_ENV": "test",
        "VALUE_1": ".env.test.local",
        "VALUE_3": ".env.test",
        "VALUE_4": ".env",
        "VALUE_5": ".env.defaults",
        "WDS_SSL": "",
        "WDS_SSL_CERT_FILE": "",
        "WDS_SSL_KEY_FILE": "",
      }
    `);
  });

  test('loads correct var when nodeEnv is "development"', () => {
    const envDir = toEnvFileVolume({
      '.env.development.local': toEnvFile({
        VALUE_1: '.env.development.local',
      }),
      '.env.local': toEnvFile({ VALUE_2: '.env.local' }),
      '.env.development': toEnvFile({ VALUE_3: '.env.development' }),
      '.env': toEnvFile({ VALUE_4: '.env' }),
      '.env.defaults': toEnvFile({ VALUE_5: '.env.defaults' }),
    });

    const result = loadEnvironment(envDir, { nodeEnv: 'development' });

    expect(result).toMatchInlineSnapshot(`
      Object {
        "CDN_HOSTNAME": "",
        "NODE_ENV": "development",
        "VALUE_1": ".env.development.local",
        "VALUE_2": ".env.local",
        "VALUE_3": ".env.development",
        "VALUE_4": ".env",
        "VALUE_5": ".env.defaults",
        "WDS_SSL": "",
        "WDS_SSL_CERT_FILE": "",
        "WDS_SSL_KEY_FILE": "",
      }
    `);
  });
});
