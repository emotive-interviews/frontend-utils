const { vol } = require('memfs');

/**
 * Records the pre-test environment variables, and provides a cleanup
 * function for restoring environment variables back to a pre-test state.
 *
 * @returns the cleanup function
 */
const createCleanupEnvFunction = () => {
  const originalEnvVars = Object.entries(process.env).reduce(
    (acc, [key, value]) => {
      acc[key] = value;
      return acc;
    },
    {},
  );

  return function cleanup() {
    Object.keys(process.env).forEach((key) => {
      if (!originalEnvVars[key]) {
        delete process.env[key];
      } else {
        process.env[key] = originalEnvVars[key];
      }
    });
  };
};

/**
 * Creates the text of an environment file from a collection of key/values.
 *
 * @param {Record<string,string>} values - the collection of values
 * @returns the environment file text
 */
function toEnvFile(values) {
  return Object.entries(values)
    .map(([key, value]) => `${key}=${value}`)
    .join('\n');
}

/**
 * Creates a mock collection of files from the specified file map.
 *
 * @param {Record<string,string>} fileMap - the map of files, where the key is the filename and the value is the file content
 * @returns the directory where the files were stored.
 */
function toEnvFileVolume(fileMap) {
  const directory = '/env-base';
  vol.fromJSON(fileMap, directory);
  return directory;
}

module.exports = { createCleanupEnvFunction, toEnvFile, toEnvFileVolume };
