const dotenv = require('dotenv');
const dotenvExpand = require('dotenv-expand');
const { fromDir } = require('./utils');

function loadEnvironment(rootPath, { nodeEnv, buildEnv }) {
  const envRoot = fromDir(rootPath);
  const loadedVars = [
    envRoot(`.env.${nodeEnv}.local`),
    nodeEnv !== 'test' && envRoot('.env.local'),
    typeof buildEnv === 'string' &&
      buildEnv.length > 0 &&
      envRoot(`.env.build.${buildEnv}`),
    envRoot(`.env.${nodeEnv}`),
    envRoot('.env'),
    envRoot('.env.defaults'),
  ]
    .filter(Boolean)
    .reduce((acc, dotenvFile) => {
      const { parsed, error } = dotenvExpand.expand(
        dotenv.config({ path: dotenvFile }),
      );
      if (error) {
        return acc;
      }
      return Object.assign({}, parsed, acc);
    }, {});

  return Object.assign(
    // Initialized defaults
    {
      CDN_HOSTNAME: process.env.CDN_HOSTNAME ?? '',
      WDS_SSL: process.env.WDS_SSL ?? '',
      WDS_SSL_CERT_FILE: process.env.WDS_SSL_CERT_FILE ?? '',
      WDS_SSL_KEY_FILE: process.env.WDS_SSL_KEY_FILE ?? '',
    },
    // Loaded environment variables
    loadedVars,
    // Forced overrides
    { NODE_ENV: nodeEnv },
  );
}

module.exports = { loadEnvironment };
