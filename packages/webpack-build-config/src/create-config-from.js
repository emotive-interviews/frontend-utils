const { merge } = require('webpack-merge');
const { createBuildConfig } = require('./build-config');
const { createEnvironment } = require('./env');

const createConfigFrom =
  (...configPartials) =>
  (
    webpackEnv,
    argv,
    { basePath = process.cwd(), partials = [], settings = {} } = {},
  ) => {
    const env = createEnvironment(basePath, argv);
    const config = createBuildConfig(basePath, env, argv, webpackEnv);
    return merge(
      configPartials
        .concat(partials)
        .map((partial) => partial(config, settings)),
    );
  };

module.exports = { createConfigFrom };
