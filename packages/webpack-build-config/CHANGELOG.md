# @emotive/webpack-build-config

## 3.2.0

### Minor Changes

- c654802: Adds remotes support to ModuleFederation integration.

## 3.1.2

### Patch Changes

- 34c5e93: Fixes issue with circular ref issues between createConfigFrom and fromDir

## 3.1.1

### Patch Changes

- 08eed3b: Removes test files from files to publish.

## 3.1.0

### Minor Changes

- e4ff079: Adds hot reload support, both for SSL and non-SSL environments

## 3.0.0

### Major Changes

- 6ca12e1: Adds support for loading environment variables from .env files matching the BUILD_ENV environment variable.

### Minor Changes

- 08a2949: Upgrades dependencies

### Patch Changes

- 3378e89: Fixes env loading issue, adds unit tests

## 2.5.2

### Patch Changes

- c2be550: Upgrades third party dependencies

## 2.5.1

### Patch Changes

- fc0da5c: Fixes issue where dev server fails to load generated assets.

## 2.5.0

### Minor Changes

- 20ae0f7: Build config dynamically determines the public path. This means that the `CDN_HOSTNAME` environment variable is no longer required. Additionally, webpack-dev-server can be used to serve federated modules.
- c62cf61: Upgrades third-party dependencies.

## 2.4.1

### Patch Changes

- 8168058: Modifies the entry points so that module extensions can be used.

## 2.4.0

### Minor Changes

- 8323714: Adds Typescript support to webpack configuration.
- bcfe206: This defaults to only `react` and `react-dom` being shared. Most libraries are too small to share, or are not intended to be shared between modules, and can significantly impact bundle size.

## 2.3.0

### Minor Changes

- 8b16021: Updates third-party dependencies.

### Patch Changes

- 8fb0deb: Fixes an issue where server-side routing doesn't work for single-page apps.

## 2.2.1

### Patch Changes

- 4fcc02b: Fixes the support for loading svgs to be correct, and moves new behavior under the .icon.svg extension.

## 2.2.0

### Minor Changes

- 73403cd: Upgrades third-party dependencies.

### Patch Changes

- 6078cac: Configures IgnorePlugin to work with Webpack schema changes

## 2.1.0

### Minor Changes

- 270950b: Adds @svgr/webpack as a dependency

### Patch Changes

- 625e563: Fixes issue where wrong version of a loader is resolved.

## 2.0.0

### Major Changes

- c55244a: Changes CSS modules to not use named exports. This may require you to change how you import CSS modules.
- 9afa192: Modifies configuration function interface to include custom partials as a config property, rather than an extra function call.

### Minor Changes

- 22b5d16: Updates the README.
- 206dde5: Refactors the collection of partials into a single "partials" directory

### Patch Changes

- fda47bf: Update supporting libraries that `webpack-build-config` uses.

## 1.2.1

### Patch Changes

- 5129868: Adds missing mini-css-extract-plugin dependency.

## 1.2.0

### Minor Changes

- eeb8038: Enables mini-css-extract-plugin when in production mode

## 1.1.0

### Minor Changes

- 4044f2d: Adds support for CSS modules
- 8c76b23: Exposes baseWebpackTemplate as a partial.
- 3d02831: Adds config to ignore moment.js locales.

## 1.0.0

### Major Changes

- a0645fc: Breaks up webpack configuration, adds more customization.

## 0.1.0

### Minor Changes

- c740100: Initial copy of webpack configuration from @emotive/navigation.
