# @emotive/webpack-build-config

Solid Webpack configuration and utilities for Emotive front end projects.

The project leverages [`webpack-merge`][webpack-merge] for combining pieces of Webpack configuration (called "partials") into a unified Webpack configuration. This makes it easy to customize an existing recommended configuration (called a "preset"), as well as to create a configuration from scratch.

## Quick Start

Install the package in your project:

```bash
npm install @emotive/webpack-build-config --save-dev
```

or

```bash
yarn add @emotive/webpack-build-config --dev # for yarn
```

Create a `webpack.config.js` file in your project directory that one of the shared factory configurations:

```js
const { createDefaultWebappConfig } = require('@emotive/webpack-build-config');

module.exports = function (webpackEnv, argv) {
  return createDefaultWebappConfig(webpackEnv, argv, {
    basePath: __dirname,
  });
};
```

## Available Factories

- `createDefaultWebappConfig`: This is the **recommended** configuration. Its configuration includes the following partials:
  - `base`
  - `environment`
  - `babel`
  - `css`
  - `fonts`
  - `images`
  - `svg`
  - `federationPlugin`
  - `manifestPlugin`
  - `serve`
- `createWebpackConfig`: This generates a base configuration that contains the following partials:
  - `base`
  - `environment`
- `createLegacyWebappConfig`: This generates a configuration suitable for legacy projects, and includes the following partials:
  - `base`
  - `environment`
  - `babel`
  - `css`
  - `fonts`
  - `images`
  - `svg`
  - `federationPlugin`
  - `manifestPlugin`
  - `resolveFromSource`
  - `serve`

## Environment Variables

`@emotive/webpack-build-config` provides a robust environment variable setup for your projects. Prior to building, the library will load your environment variables from the following files, ordered by precedence:

- `.env.{NODE_ENV}.local` _(valid values are `devlopment`, `test`, `production`)_
- `.env.local` _(except when `NODE_ENV` is `test`)_
- `.env.build.{BUILD_ENV}` _(valid values are `review`, `dev`, `stg`, `prd`)_
- `.env.{NODE_ENV}`
- `.env`
- `.env.defaults`

If a variable is specified in multiple files, the one the in the file with the highest precedence will be used.

**NOTE**: Not all files are required! If an environment file is missing, it will be ignored.

### Environment Variable Recommendations

1. Add a `.env.defaults` file to your project, and commit it. As of version 3.0.0, only environment variables included in the loaded environment variable files (and `NODE_ENV` and `CDN_HOSTNAME`) will be available to Webpack. Keeping the defaults in this file will make your project easier to configure and build.
2. Use variable expansion to include environment variables from external providers (e.g., Gitlab environment variables). The project uses [dotenv-expand][dotenv-expand] to support this functionality. Refer to the that project's [README][dotenv-expand] for more information about how to use variable expansion.
3. Leverage the `.env.build.{BUILD_ENV}` files for loading environment variables for different CI environments. Since all CI build will use `NODE_ENV` of `production`, the `.env.{NODE_ENV}` file provides little value in the situation.
4. Do not check in `.env.local` or `.env.{NODE_ENV}.local` files. Those files are intended for variables specific to an individual's local environment.

In summary, limit the number of `.env` files you commit in your project. Include a `.env.defaults` file to ensure that your project has the set of variables that it needs, but then consider getting the rest of the values via variable expansion (for CI build system) or `.env.local` files (for local development).

## Configuring the Webpack Dev Server to Support SSL

If the following environment variables are provided, then the webpack dev server will be configured to support SSL:

- `WDS_SSL`: set to `true` to enable SSL support
- `WDS_SSL_CERT_FILE`: the path to the SSL cert file
- `WDS_SSL_KEY_FILE`: the path to the SSL key file

Example:

```
# in a .env.local file
WDS_SSL=true
WDS_SSL_CERT_FILE=localhost.pem
WDS_SSL_KEY_FILE=localhost-key.pem
```

### How to Generate the Necessary Certificates

Generate your local certificate using [mkcert][mkcert]. Refer to mkcert's [README][mkcert] for installation instructions.

Once done, then create a `localhost` cert using the following commands:

```bash
mkcert -install # adds the necessary root certs to your workstation
mkcert localhost # generates localhost cert files.
```

### Recommendations

Generate the `localhost` certs into a known location, and then refer to the location using file expansion in a `.env.local` file in the project. For example, suppose the localhost certs are generated in your home directory:

```
# .env.local
WDS_SSL=true
WDS_SSL_CERT_FILE=$HOME/localhost.pem
WDS_SSL_KEY_FILE=$HOME/localhost-key.pem
```

## Partials

Partials are portions of a Webpack configuration that can be composed to combine into a full Webpack configuration.

Example:

```js
// webpack.config.js
const {
  createWebpackConfig,
  partials: { babel, css },
} = require('@emotive/webpack-build-config');

module.exports = function (webpackEnv, argv) {
  return createWebpackConfig(webpackEnv, argv, {
    basePath: __dirname,
    partials: [babel, css], // adds Babel and CSS configuration to the Webpack configuration.
  });
};
```

### Available "built-in" Partials

- `babel`: Enables Babel support for the project. Assumes that there is a Babel configuration defined in the project.
- `base`: The base Webpack configuration that all configurations are built from. This is already included in any of the available configuration factories.
- `css`: Enables CSS and Sass support in the project. Also configures `style-loader` in when in development mode and `MiniCssExtractPlugin` when in production mode.
- `environment`: Enables environment-specific configuration values for `mode` and `devtool`. This is already included in any of the available configuration factories.
- `federationPlugin`: Enables module federation support in the project. It assumes that any module to be exposed is defined in the `src/interfaces` directory of the project.
  - NOTE: By default `react` and `react-dom` libraries are shared between federated modules. If you would like to share more libraries, specify using the `settings.federationPlugin.shared` array.
- `fonts`: Provides asset support for fonts.
- `images`: Provides assets support for non-vector (SVG) images.
- `manifestPlugin`: Enabled the Webpack Manifest plugin for the project, which provides a list of generated assets.
- `resolveFromSource`: Configures the project to support resolving files from the `src` directory. This means that projects can access resources without using relative paths.
- `serve`: Enables webpack dev server support in the project.
- `svg`: Configures SVG assets to be loaded as React components using `SVGR`.

### Writing Your Own Partial

The partial interface is a function that accepts a single `config` argument. A partial should either return the portion(s) of the Webpack configuration it expects to add, or an empty object (`{}`) or `undefined` if it has not changes to add.

Example Partial:

```js
function babelPartial(config) {
  return {
    module: {
      rules: [
        {
          test: /\.(js|jsx)$/,
          exclude: /node_modules/,
          use: 'babel-loader',
        },
      ],
    },
  };
}

// Usage:
const { createWebpackConfig } = require('@emotive/webpack-build-config');

module.exports = function (webpackEnv, argv) {
  return createWebpackConfig(webpackEnv, argv, {
    basePath: __dirname,
    partials: [babelPartial],
  });
};
```

## Settings

The settings property contains values to configure the behavior of certain partials.

Example:

```js
// webpack.config.js
const { createWebpackConfig } = require('@emotive/webpack-build-config');

module.exports = function (webpackEnv, argv) {
  return createWebpackConfig(webpackEnv, argv, {
    basePath: __dirname,
    settings: {
      federationPlugin: {
        shared: ['lodash'], // adds "lodash" as a shareable library.
      },
    },
  });
};
```

### Available Settings

- `federationPlugin.shared` _(`string[]`)_: An array of dependencies that should be marked as shareable for the given federated module. This allows other federated modules who also mark this module as shareable to re-use the same copy.
- `federationPlugin.remotes` _(`string[]`)_: An array of federated modules that should be made available within the bundle as a remote. This allows one to import a federated module from another project.

The `config` object contains the following information that can be used to configure a partial:

- `env` _(`object`)_: A collection of methods for accessing and using environment variables.
  - `env.envVar` _(`(varName, defaultValue) => any`)_: Access the value of an environment variables. Defaults to `defaultValue` when not found.
  - `env.serialize` _(`() => Object<string,string>`)_: Generates a serialized version of all environment variables for the project. Primarily used for making the values available for the Webpack `DefinePlugin` plugin.
- `nodeEnv` _(`string`)_: The `process.env.NODE_ENV` value for the build. Is usually `development` or `production`.
- `build` _(`boolean`)_: Is `true` if a build is being done, otherwise `false`.
- `serve` _(`boolean`)_: Is `true` if the app is being served (using `webpack-dev-server`), otherwise `false`.
- `project` _(`object`)_: An object containing the project name formatted in a variety of formats:
  - `project.name` _(`string`)_: The project name, sans `@emotive/` prefix.
  - `project.camelCase` _(`string`)_: The project name, sans `@emotive/` prefix, in camel-case format.
  - `project.paramCase` _(`string`)_: The project name, sans `@emotive/` prefix, in param-case (kebab-case) format.
- `publicPath` _(`string`)_: Contains the application's public path.
- `rootDir` _(`(...paths: string[]) => string`)_: A function that can be used to chain directory values to the root directory of the project. When invoked with no arguments, it will return the root directory path.
- `srcDir` _(`(...paths: string[]) => string`)_: A function that can be used to chain directory values to the source directory of the project. When invoked with no arguments, it will return the source directory path.
- `destDir` _(`(...paths: string[]) => string`)_: A function that can be used to chain directory values to the destination directory of the project. When invoked with no arguments, it will return the destination directory path.
- `pkg` _(`object`)_: The `package.json` for the project.

[dotenv-expand]: https://github.com/motdotla/dotenv-expand
[mkcert]: https://github.com/FiloSottile/mkcert
[webpack-merge]: https://www.npmjs.com/package/webpack-merge
